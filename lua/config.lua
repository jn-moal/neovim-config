local opt, glob, cmd = vim.o, vim.g, vim.cmd
local indent = 4

opt.encoding = 'utf-8'                                      -- File encoding
opt.expandtab = true                                        -- Use space instead of tabs
opt.tabstop = indent                                        -- Default to 2 spaces for tabs
opt.autoindent = true                                       -- Enable auto indent
opt.shiftwidth = indent                                     -- Space for indention operation (>> or <<)
opt.hidden = true                                           -- Enable background buffers
opt.autoread = true                                         -- Automatically read changed files
opt.autowrite = true                                        -- Automatically write files before :make, :next etc...
opt.cursorline = true                                       -- Highlight cursor line
opt.termguicolors = true                                    -- Enable true colors
opt.number = true                                           -- Display line number
opt.pumheight = 10                                          -- Completion window max size
opt.undofile = true                                         -- Unable undo file even after exiting Neovim
opt.undodir = os.getenv('HOME')..'/.config/nvim/tmp/undo//' -- Configure undo cache
opt.splitright = true                                       -- Vertical windows should be split to the right
opt.splitbelow = true                                       -- Horizontal windows should be split to the bottom
opt.guicursor = 'n-v-c-sm-i-ci-ve-r-cr-o:hor22'             -- Set horizontal cursor
opt.completeopt = "menuone,noinsert,noselect"               -- Show popup menu, even if there is one entry
opt.fileformats = "unix"                                    -- Set file formats preference

glob.mapleader = "," -- Remap leader

-- Disable swapfile
cmd 'set noswapfile'

-- Enables copy to clipboard for operation like yank, delete, change...
cmd 'set clipboard^=unnamed'
cmd 'set clipboard^=unnamedplus'


cmd 'set noshowmode'   -- Disable Neovim default status bar, we use vim-airline instead
cmd 'syntax enable'
cmd 'colorscheme nord' -- Set neovim theme

-- Sneak displays the shortcut to use to move to word
cmd 'let g:sneak#label = 1'

-- Go import ordered on save with LSP
function GoImports(timeout_ms)
  local context = { source = { organizeImports = true } }
  vim.validate { context = { context, "t", true } }

  local params = vim.lsp.util.make_range_params()
  params.context = context

  -- See the implementation of the textDocument/codeAction callback
  -- (lua/vim/lsp/handler.lua) for how to do this properly.
  local result = vim.lsp.buf_request_sync(0, "textDocument/codeAction", params, timeout_ms)
  if not result or next(result) == nil then return end
  local actions = result[1].result
  if not actions then return end
  local action = actions[1]

  -- textDocument/codeAction can return either Command[] or CodeAction[]. If it
  -- is a CodeAction, it can have either an edit, a command or both. Edits
  -- should be executed first.
  if action.edit or type(action.command) == "table" then
    if action.edit then
      vim.lsp.util.apply_workspace_edit(action.edit)
    end
    if type(action.command) == "table" then
      vim.lsp.buf.execute_command(action.command)
    end
  else
    vim.lsp.buf.execute_command(action)
  end
end

vim.api.nvim_command("autocmd BufWritePre *.go lua GoImports(1000)")

