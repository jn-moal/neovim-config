local nvimlsp = require('lspconfig')

-- function attach completion when setting up lsp
local custom_on_attach = function(client, bufnr)

  local opts = { noremap=true, silent=true }

  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  -- Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  buf_set_keymap('n', '<leader>cr', '<CMD>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', '<leader>cD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap("n", "<leader>cf", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)

  require('completion').on_attach(client)

end

-- Enable diagnostics
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    virtual_text = true,
    signs = true,
    update_in_insert = true,
  }
)

-- Setting up golang language server
nvimlsp.gopls.setup({ on_attach = custom_on_attach })

-- Setting up terraform language server
nvimlsp.terraformls.setup({ on_attach = custom_on_attach })

-- Setting up rust language server
nvimlsp.rust_analyzer.setup({ on_attach = custom_on_attach })

-- Setting up python language server
nvimlsp.pylsp.setup({ on_attach = custom_on_attach })

-- Setting up bash language server
nvimlsp.bashls.setup({ on_attach = custom_on_attach })

-- Setting up ruby language server
nvimlsp.solargraph.setup({ on_attach = custom_on_attach })

-- Setting up lua language server
local sumneko_root_path = os.getenv('HOME')..'/Development/neovim/lua-language-server'
local sumneko_binary = sumneko_root_path.."/bin/Linux/lua-language-server"

-- Setting up all path for lua LSP (Neovim config, Neovim plugins...)
local library = {}
local function add(lib)
  for _, p in pairs(vim.fn.expand(lib, false, true)) do
    p = vim.loop.fs_realpath(p)
    library[p] = true
  end
end

local path = vim.split(package.path, ';')

table.insert(path, "lua/?.lua")
table.insert(path, "lua/?/init.lua")

add("$VIMRUNTIME") -- Add neovim runtime
add("~/.config/nvim") -- Add neovim config

-- Add packer library
add("~/.local/share/nvim/site/pack/packer/start/*")

nvimlsp.sumneko_lua.setup {
  on_attach = custom_on_attach,
  -- delete root from workspace to make sure we don't trigger duplicate warnings
  on_new_config = function(config, root)
    local libs = vim.tbl_deep_extend("force", {}, library)
    libs[root] = nil
    config.settings.Lua.workspace.library = libs
    return config
  end,

  cmd = {sumneko_binary, "-E", sumneko_root_path .. "/main.lua"},
  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT',
        -- Setup your lua path
        path = path
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = {'vim'},
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = library,
        maxPreload = 2000,
        preloadFileSize = 50000
      },
      -- Do not send telemetry data containing a randomized but unique identifier
      telemetry = {
        enable = false,
      },
    },
  },
}
