local function map(mode, lhs, rhs, opts)
  local options = {noremap = true}
  if opts then
    options = vim.tbl_extend("force", options, opts)
  end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

local opt = {}

-- Buffers mapping
map("n", "<C-p>", [[<CMD> BufferPrevious<CR>]], opt)  -- Open previous buffer
map("n", "<C-n>", [[<CMD> BufferNext<CR>]], opt)      -- Open next buffer
map("n", "<C-w>", [[<CMD> BufferClose<CR>]], opt)     -- Delete (close) current buffer

-- Mapping for Navigator plugin
map('n', "<C-h>", "<CMD>lua require('Navigator').left()<CR>", opt)
map('n', "<C-k>", "<CMD>lua require('Navigator').up()<CR>", opt)
map('n', "<C-l>", "<CMD>lua require('Navigator').right()<CR>", opt)
map('n', "<C-j>", "<CMD>lua require('Navigator').down()<CR>", opt)

-- Mapping for telescope plugin
map("n", "<C-f>f", [[ <CMD>Telescope find_files<CR> ]], opt)                                                         -- map Ctrl+f f to find files
map("n", "<C-f>", [[ <CMD> lua require('telescope.builtin').live_grep() <CR> ]], opt)
map("n", "<C-f>s", [[ <CMD> lua require('session-lens').search_session() <CR> ]], opt)
map("n", "<leader>s", [[ <CMD> lua require('session_manager').save_session() <CR> ]], opt)
map("n", "<leader>cd", [[ <CMD> lua require('telescope.builtin').lsp_definitions() <CR> ]], opt)
map("n", "<leader>ca", [[ <CMD> lua require('telescope.builtin').lsp_code_actions() <CR> ]], opt)
map("n", "<leader>ci", [[ <CMD> lua require('telescope.builtin').lsp_implementations() <CR> ]], opt)
map("n", "<leader>cC", [[ <CMD> lua require('telescope.builtin').lsp_workspace_diagnostics() <CR> ]], opt)
map("n", "<leader>cS", [[ <CMD> lua require('telescope.builtin').lsp_dynamic_workspace_symbols() <CR> ]], opt)
map("n", "<leader>cs", [[ <CMD> lua require('telescope.builtin').lsp_document_symbols() <CR> ]], opt)
map("n", "<leader>gh", [[ <CMD> lua require('telescope.builtin').git_commits() <CR> ]], opt)
map("n", "<leader>gs", [[ <CMD> lua require('telescope.builtin').git_status() <CR> ]], opt)

-- Mapping for Tree Explorer
map("n", "f", [[ <CMD> NvimTreeToggle<CR> ]], opt)
map("n", "<leader>r", [[ <CMD> NvimTreeRefresh<CR> ]], opt)

-- FTerm mappings
map('n', '<C-t>', [[<CMD>lua require("FTerm").toggle()<CR>]], opt)
map('t', '<C-t>', [[<CMD>lua require("FTerm").toggle()<CR>]], opt)

require('FTerm').setup({
    dimensions = {
      width = 0.95,
      height = 0.95,
      x = 0.5,
      y = 0.5
    }
})

vim.api.nvim_command([[
autocmd BufWritePre * :lua vim.lsp.buf.formatting()
]])

