local nvimgo = {}
local execute = vim.api.nvim_command

function nvimgo.build()
  print('Building...')
  execute('!go build')
  print('Built')
end

function nvimgo.test()
  print('Testing...')
  execute('!go test ./...')
  print('Tested')
end

return nvimgo
