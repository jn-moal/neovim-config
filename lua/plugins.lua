-- Ensure packer is installed
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
end

-- Setting up packer Neovim plugin manager
return require('packer').startup({
  function(use)
    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- Theme
    use{'arcticicestudio/nord-vim'}       -- Nord theme

    -- TreeSitter for better highlighting
    use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate',
        config = function()
            require('nvim-treesitter.configs').setup {
                ensure_installed = {'go', 'rust', 'yaml', 'lua'},
                highlight = {
                    enable= true,
                },
            }
        end
    }

    -- Neovim package that integrates with its built-in LSP client
    use {
      'nvim-lua/lsp_extensions.nvim', -- A bunch of info & extension callbacks
      requires = {
        {'neovim/nvim-lspconfig'},    -- Collection of common configuration
        {'nvim-lua/completion-nvim'}, -- Auto-completion
        {'nvim-lua/lsp-status.nvim'}, -- Add LSP status information in status line
        {
          'folke/lsp-colors.nvim',
          config = function() 
                require('lsp-colors').setup({ Error = "#db4b4b", Warning = "#e0af68", Information = "#0db9d7", Hint = "#10B981" })
                require('lsp-status').register_progress()
          end
        },
      }
    }

    -- barbar plugin display tabline
    use{
      'romgrk/barbar.nvim',            -- tabline plugin
      requires = {
        'kyazdani42/nvim-web-devicons' -- tabline icons
      }
    }

    -- Status line extension
    use{
        'nvim-lualine/lualine.nvim',
        requires = {'kyazdani42/nvim-web-devicons', opt = true},
        config = function() require('lualine').setup() end
    }

    -- Vim rooter (set working directory to git root folder)
    use{'airblade/vim-rooter'}

    -- Telescope plugins and dependencies
    use{'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
    use{
      'nvim-telescope/telescope.nvim', -- Extensible fuzzy finder over lists
      requires = {
        {'nvim-lua/popup.nvim'},       -- Implements popup API from Vim in Neovim
        {'nvim-lua/plenary.nvim'},     -- Usefull lua functions
        {'rmagatti/auto-session'},
        {'rmagatti/session-lens'},
      },
      config = function()
            require('telescope').setup{}
            require('telescope').load_extension('session-lens')
            require('telescope').load_extension('fzf')
        end
    }

    -- Easy vim motion
    use{'justinmk/vim-sneak'}

    -- Indent line displays vertical line for space indented code
    use{'Yggdroot/indentLine'}

    -- Easily align text based on character
    use{'godlygeek/tabular'}

    -- Tree Explorer
    use{
        'kyazdani42/nvim-tree.lua',
        config = function() require('nvim-tree').setup {
            view = {
                width = 60,
                side = 'right',
            },
            update_focused_file = {
                enable = true,
            }
        } end
    }

    use {
        'tanvirtin/vgit.nvim',
        requires = 'nvim-lua/plenary.nvim',
        config = function() require('vgit').setup() end
    }

    -- EditorConfig plugin
    use {'editorconfig/editorconfig-vim'}

    -- Floating terminal
    use {
      'numtostr/FTerm.nvim',
    }

    use {
      'numToStr/Navigator.nvim',
      config = function()
          require('Navigator').setup()
      end
    }

    -- Setting up language specific plugins
    use{'hashivim/vim-terraform', ft='terraform'} -- Add hashivim terraform plugin (color synthaxe, file type detection)

    use{'habamax/vim-asciidoctor', ft='asciidoctor'}

  end,
  config = {
    display = {
      open_fn = require('packer.util').float,
    }
  }
})

