require('plugins')  -- Loading plugins
require('config')   -- Loading Neovim configuration
require('mappings') -- Loading mapping
require('language') -- Loading LSPs configuration
